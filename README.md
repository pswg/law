# Supreme Law of Fountainhead

<!-- Preamble -->
This repository, located at https://gitlab.com/impyr.io/fountainhead/law, is
the Official Constitution of Fountainhead and the supreme law of the land. Any 
code contained herein shall have the full force of law.

## The Server

There shall be an HTTP server providing public access to Fountainhead.
